<?php

$filepath = 'assets\file.txt';
$data = false;
$fileExists = file_exists($filepath);
if($fileExists){
    $data = file_get_contents($filepath,true);
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master</title>
    <link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

    <section class="section">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="/blog">
                            Blog (CRUD)
                        </a>

                        <a class="navbar-item" href="/file">
                            File
                        </a>

                        <a class="navbar-item" href="/image">
                            Image
                        </a>

                    </div>

                </div>
            </nav>
            <hr>
            <a href="/file/create-update" class="button is-primary is-small">Create/Edit</a>
            <form action="/file" method="POST" style="display: inline;">
                <input type="hidden" name="_method" value="delete" />
                <button type="submit" class="button is-danger is-small">Delete</button>
            </form>

            <hr>
            <div class="field">
                <div class="control">
                    <textarea class="textarea" disabled><?php if($data) echo($data); else echo('No data found'); ?></textarea>
                </div>
            </div>
        </div>
    </section>
</body>

</html>