<?php

$filepath = 'assets\file.txt';
$data = false;
$fileExists = file_exists($filepath);
if ($fileExists) {
    $data = file_get_contents($filepath, true);
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master</title>
    <link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

    <section class="section">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="/blog">
                            Blog (CRUD)
                        </a>

                        <a class="navbar-item" href="/file">
                            File
                        </a>

                        <a class="navbar-item" href="/image">
                            Image
                        </a>

                    </div>

                </div>
            </nav>
            <hr>
            <a href="/file" class="button is-primary is-small">Back</a>
            <hr>
            <form action="/file" method="POST">
                <div class="field">
                    <label class="label">Text File</label>
                    <div class="control">
                        <textarea class="textarea" name="content" rows="15"><?php if($data) echo($data);?></textarea>

                    </div>
                </div>
                <button type="submit" class="button is-link">Save</button>
            </form>
        </div>
    </section>
</body>

</html>