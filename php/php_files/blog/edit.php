<?php
if (isset($_GET) && isset($_GET['id'])) {
    $blog_id = $_GET['id'];
    $row;
    $servername = "localhost";
    $username = "root";
    $password = "57Arga-7G";
    $dbname = "project_php";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }
    
    $sql = "SELECT id, title, body FROM `blogs` WHERE id = " . $blog_id . ";";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
    } else {
        die('0 results');

    }
    mysqli_close($conn);
} else {
    die('not found blog id');
}



?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master</title>
    <link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

    <section class="section">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="/blog">
                            Blog (CRUD)
                        </a>

                        <a class="navbar-item" href="/file">
                            File
                        </a>

                        <a class="navbar-item" href="/image">
                            Image
                        </a>

                    </div>

                </div>
            </nav>
            <hr>
            <a href="/blog" class="button is-primary is-small">Back</a>
            <hr>
            <form action="/blog/update?id=<?php echo($blog_id); ?>" method="POST">
                <div class="field">
                    <label class="label">Title</label>
                    <div class="control">
                        <input class="input" type="text" name="title" value="<?php echo($row['title']); ?>">
                    </div>
                </div>
                <div class="field">
                    <label class="label">Body</label>
                    <div class="control">
                        <textarea class="textarea" name="body" rows="15"><?php echo($row['body']); ?></textarea>

                    </div>
                </div>


                <button type="submit" class="button is-link">Save</button>
            </form>

        </div>
    </section>
</body>

</html> 