<?php
$fileList = glob('assets/images/*');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master</title>
    <link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

    <section class="section">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="/blog">
                            Blog (CRUD)
                        </a>

                        <a class="navbar-item" href="/file">
                            File
                        </a>

                        <a class="navbar-item" href="/image">
                            Image
                        </a>

                    </div>

                </div>
            </nav>
            <hr>
            <form action="/image" method="POST" enctype="multipart/form-data">
                <h1>Image Upload</h1>
                <div class="field">
                    <label class="label">Image</label>
                    <div class="control">
                        <input class="input" type="file" name="image">
                    </div>
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <button type="submit" name="submit" class="button is-success">Save</button>
                    </div>
                </div>

            </form>

            <hr>
            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                    <tr>
                        <th>File Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($fileList as $key => $value) {
                    ?>
                        <tr>
                            <td><?php echo ($value); ?></td>
                            <td>
                                <a href="<?php echo ($value); ?>" target="_blank" class="button is-info is-small">Show</a>
                                <form action="/image?path=<?php echo ($value); ?>" method="POST" style="display: inline;">
                                    <input type="hidden" name="_method" value="delete" />
                                    <button type="submit" class="button is-danger is-small">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </section>
</body>

</html>