<?php
if (isset($_GET) && isset($_GET['path'])) {
    $filepath = $_GET['path'];
    $fileExists = file_exists($filepath);
    if ($fileExists) {
        unlink($filepath);
    }
}

$host = $_SERVER['HTTP_HOST'];
$uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'image';
// header("Location: http://$host$uri/$extra");
