<?php

switch ($_SERVER['REDIRECT_URL']) {
        /** Start File Section */
    case '/file':
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['_method']) && ($_POST['_method'] == 'delete')) {
                require_once 'php_files/file/delete.php';
            } else {
                require_once 'php_files/file/store.php';
            }
        } else {
            require_once 'php_files/file/index.php';
        }
        break;
    case '/file/create-update':
        require_once 'php_files/file/create.php';
        break;
        /** End File Section */

        /** Start Image Section */
    case '/image':
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['_method']) && ($_POST['_method'] == 'delete')) {
                require_once 'php_files/image/delete.php';
            } else {
                require_once 'php_files/image/store.php';
            }
        } else {
            require_once 'php_files/image/index.php';
        }
        break;
        /** End Image Section */

        /** Start Blog Section */
    case '/blog':
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['_method']) && ($_POST['_method'] == 'delete')) {
                require_once 'php_files/blog/delete.php';
            } else {
                require_once 'php_files/blog/store.php';
            }
        } else {
            require_once 'php_files/blog/index.php';
        }
        break;
    case '/blog/json':
        require_once 'php_files/blog/json.php';
        break;
    case '/blog/create':
        require_once 'php_files/blog/create.php';
        break;
    case '/blog/edit':
        require_once 'php_files/blog/edit.php';
        break;
    case '/blog/update':
        require_once 'php_files/blog/update.php';
        break;
    case '/blog/delete':
        require_once 'php_files/blog/delete.php';
        break;
        /** End Blog Section */


    default:
        die('No Page Found');
        break;
}
