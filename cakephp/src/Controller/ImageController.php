<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;

/**
 * Image Controller
 *
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImageController extends AppController
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dir = new Folder(WWW_ROOT . 'asset/images');
        $files = $dir->find();

        $this->set(compact('files'));
        return $this->render('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
            $fileobject = $this->request->getData('image');
            $uploadPath = 'asset/images/';
            $destination = $uploadPath.$fileobject->getClientFilename();
            // Existing files with the same name will be replaced.
            $fileobject->moveTo($destination);
        exit();

            // return $this->redirect(['action' => 'index']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $filename = 'asset/images/' . $this->request->getQuery('path');

        $file = new File($filename);
        $data = false;
        if (file_exists($filename)) {
            $data = $file->delete();
        }
        exit();

        // return $this->redirect(['action' => 'index']);

    }

}
