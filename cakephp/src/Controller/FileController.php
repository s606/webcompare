<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Filesystem\File;

/**
 * File Controller
 *
 * @method \App\Model\Entity\File[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FileController extends AppController
{
    public function index()
    {
        $filename = 'asset/file.txt';

        $file = new File($filename);
        $data = false;
        if (file_exists($filename)) {
            $data = $file->read();
        }

        $this->set(compact('data'));
        return $this->render('index');
    }

    public function create()
    {
        $filename = 'asset/file.txt';

        $file = new File($filename);
        $data = false;
        if (file_exists($filename)) {
            $data = $file->read();
        }

        $this->set(compact('data'));
        return $this->render('create');
    }

    public function store()
    {
        $this->request->allowMethod(['post']);
        $filename = 'asset/file.txt';

        $file = new File($filename);
        $data = $file->write($this->request->getData('content'));
        exit();

        // return $this->redirect(['action' => 'index']);
    }


    public function delete()
    {
        $this->request->allowMethod(['post']);
        $filename = 'asset/file.txt';

        $file = new File($filename);
        $data = false;
        if (file_exists($filename)) {
            $data = $file->delete();
        }
        exit();

        // return $this->redirect(['action' => 'index']);
    }
}
