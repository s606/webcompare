<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Blogs Controller
 *
 * @property \App\Model\Table\BlogsTable $Blogs
 * @method \App\Model\Entity\Blog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BlogsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $Blogs = $this->Blogs->find('all');
        $this->set('Blogs', $Blogs);
        return $this->render('index');
    }
    public function json()
    {
        $Blogs = $this->Blogs->find('all');
        echo json_encode($Blogs);
        exit;
    }


    public function create()
    {
        return $this->render('create');
    }

    public function store()
    {
        $this->request->allowMethod(['post']);

        $Blog = $this->Blogs->newEmptyEntity();
        $Blog = $this->Blogs->patchEntity($Blog, $this->request->getData());

        $this->Blogs->save($Blog);
        exit();
        // return $this->redirect(['action' => 'index']);
    }

    public function edit()
    {
        $id = $this->request->getQuery('id');
        $blog = $this->Blogs->get($id);
        $this->set('Blog', $blog);

        return $this->render('edit');
    }
    public function update()
    {
        $id = $this->request->getQuery('id');
        $blog = $this->Blogs->get($id);
        $this->Blogs->patchEntity($blog, $this->request->getData());
        $this->Blogs->save($blog);
        exit();

        // return $this->redirect(['action' => 'index']);
    }

    public function delete()
    {
        $this->request->allowMethod(['post']);

        $id = $this->request->getQuery('id');
        $blog = $this->Blogs->get($id);
        $this->Blogs->delete($blog);
        exit();

        // return $this->redirect(['action' => 'index']);
    }

}
