<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
/** @var \Cake\Routing\RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    $builder->get('/file', ['controller' => 'File', 'action' => 'index']);
    $builder->post('/file/delete', ['controller' => 'File', 'action' => 'delete']);
    $builder->get('/file/create', ['controller' => 'File', 'action' => 'create']);
    $builder->post('/file', ['controller' => 'File', 'action' => 'store']);

    $builder->get('/image', ['controller' => 'Image', 'action' => 'index']);
    $builder->post('/image/delete', ['controller' => 'Image', 'action' => 'delete']);
    $builder->post('/image', ['controller' => 'Image', 'action' => 'store']);

    $builder->get('/blog', ['controller' => 'Blogs', 'action' => 'index']);
    $builder->get('/blog/json', ['controller' => 'Blogs', 'action' => 'json']);
    $builder->get('/blog/create', ['controller' => 'Blogs', 'action' => 'create']);
    $builder->post('/blog', ['controller' => 'Blogs', 'action' => 'store']);
    $builder->get('/blog/edit', ['controller' => 'Blogs', 'action' => 'edit']);
    $builder->post('/blog/update', ['controller' => 'Blogs', 'action' => 'update']);
    $builder->post('/blog/delete', ['controller' => 'Blogs', 'action' => 'delete']);

    // Route::resource('blog', 'BlogController')->except(['show']);


    $builder->fallbacks();
});
