<form action="/image" method="POST" enctype="multipart/form-data">
    <h1>Image Upload</h1>
    <div class="field">
        <label class="label">Image</label>
        <div class="control">
            <input class="input" type="file" name="image">
        </div>
      </div>
      <div class="field is-grouped">
        <div class="control">
            <button type="submit" class="button is-success">Save</button>
        </div>
      </div>

</form>

<hr>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($files as $file) {
            ?>
                    <tr>
            <td><?= $file ?></td>
            <td>
                <a href="/asset/images/<?= $file ?>" target="_blank" class="button is-info is-small">Show</a>
                <form action="/image/delete/?path=<?= $file ?>" method="POST" style="display: inline;">
                    <button type="submit" class="button is-danger is-small">Delete</button>
                </form>
            </td>
        </tr>

            <?php
        }
        ?>



    </tbody>
</table>
