<a href="/file" class="button is-primary is-small">Back</a>
    <hr>
    <form action="/file" method="POST">

        <div class="field">
            <label class="label">Text File</label>
            <div class="control">
                <textarea class="textarea" name="content" rows="15"><?= $data ?></textarea>

            </div>
        </div>
        <button type="submit" class="button is-link">Save</button>
    </form>
