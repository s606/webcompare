<a href="/file/create" class="button is-primary is-small">Create/Edit</a>
<form action="/file/delete" method="POST" style="display: inline;">
    <button type="submit" class="button is-danger is-small">Delete</button>
</form>

<hr>
<div class="field">
    <div class="control">
        <textarea class="textarea" disabled><?= $data ?></textarea>
    </div>
</div>
