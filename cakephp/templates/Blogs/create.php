<a href="/blog" class="button is-primary is-small">Back</a>
<hr>
<form action="/blog" method="POST">
    <div class="field">
        <label class="label">Title</label>
        <div class="control">
            <input class="input" type="text" name="title">
        </div>
    </div>
    <div class="field">
        <label class="label">Body</label>
        <div class="control">
            <textarea class="textarea" name="body"></textarea>

        </div>
    </div>


    <button type="submit" class="button is-link">Save</button>
</form>

