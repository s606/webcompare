<a href="/blog" class="button is-primary is-small">Back</a>
<hr>
<form action="/blog/update?id=<?= $Blog->id ?>" method="POST">
    <div class="field">
        <label class="label">Title</label>
        <div class="control">
            <input class="input" type="text" name="title" value="<?= $Blog->title ?>">
        </div>
    </div>
    <div class="field">
        <label class="label">Body</label>
        <div class="control">
            <textarea class="textarea" name="body" rows="15"><?= $Blog->body ?></textarea>

        </div>
    </div>


    <button type="submit" class="button is-link">Update</button>
</form>
