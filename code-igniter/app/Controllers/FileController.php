<?php

namespace App\Controllers;

class FileController extends BaseController
{
	public function index()
	{
		$filename = 'assets/file.txt';
		$data = false;

		if (file_exists($filename)) {
			$data = file_get_contents($filename);
		}
		return view('file/index', ['data' => $data]);
	}

	public function destroy()
	{
		$filename = 'assets/file.txt';
		if (file_exists($filename)) {
			unlink($filename);
		}
		// exit();
		
		// return redirect()->to('/file');
		
	}


	public function create()
	{
		$filename = 'assets/file.txt';
		$data = false;

		if (file_exists($filename)) {
			$data = file_get_contents($filename);
		}
		return view('file/create', ['data' => $data]);
	}
	public function store()
	{
		$filename = 'assets/file.txt';

		if ($this->request->getMethod() === 'post' && $this->validate([
			'content' => 'required',
		])) {
			$file = fopen($filename, "w") or exit("Unable to open file!");
			$data = $this->request->getPost('content');

			fwrite($file, $data);
		// exit();
			
			// return redirect()->to('/file');
		}
	}
}
