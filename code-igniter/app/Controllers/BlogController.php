<?php

namespace App\Controllers;

use App\Models\Blog;

class BlogController extends BaseController
{
	public function index()
	{
		$model = new Blog();
		$blogs = $model->getBlogs();

		return view('blog/index', ['blogs' => $blogs]);
	}

	public function create()
	{
		return view('blog/create');
	}

	public function store()
	{
	$this->autoRender = false;
		
		$validate = $this->validate([
			'title' => 'required',
			'body'  => 'required',
		]);

		if ($this->request->getMethod() === 'post' && $validate) {
			$model = new Blog();

			$model->save([
				'title' => $this->request->getPost('title'),
				'body'  => $this->request->getPost('body'),
			]);
		}
		return ;
		// exit();
		
		// return redirect()->to('/blog');
	}
	public function edit()
	{
		$model = new Blog();
		$modelID = $this->request->getGet('id');
		$blog = $model->find($modelID);
		if (!$blog) {
			return redirect()->to('/blog');
		}
		return view('blog/edit', ['blog' => $blog]);
	}
	public function update()
	{	
		$validate = $this->validate([
			'title' => 'required',
			'body'  => 'required',
		]);
		if ($this->request->getMethod() === 'post' && $validate) {
			$model = new Blog();
			$modelID = $this->request->getPost('id');
			$blog = $model->find($modelID);
			if ($blog) {
				$data['title'] = $this->request->getPost('title');
				$data['body'] = $this->request->getPost('body');
				
				$model->update($modelID, $data);

			}
		}
		// exit();
		// return redirect()->to('/blog');
	}


	public function destroy()
	{
	$this->autoRender = false;
		
		$model = new Blog();
		$modelID = $this->request->getPost('id');
		$blog = $model->find($modelID);
		if ($blog) {
			$model->delete($modelID);
		}
		// exit();
		
		// return redirect()->to('/blog');
	}
	
	public function json()
	{
		header('Content-Type: application/json');
		$model = new Blog();
		$blogs = $model->getBlogs();

	  
		echo json_encode($blogs);
		}

  
  
}
