<?php

namespace App\Controllers;

class ImageController extends BaseController
{
	public function index()
	{
		$fileList = glob('assets/images/*');
		return view('image/index', ['fileList' => $fileList]);
	}

	public function destroy()
	{
		if ($this->request->getGet('path')) {
			unlink($this->request->getGet('path'));
		}
		// exit();
		
		// return redirect()->to('/image');
	}


	public function store()
	{

 

		if ($this->request->getMethod() === 'post' && $this->validate([
			'file' => [
                'uploaded[file]',
                'mime_in[file,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[file,4096]',
			],
		])) {
	
				$file = $this->request->getFile('file');
				$file->move('assets/images');
		}
		// exit();
		
		// return redirect()->to('/image');
		
	}
}
