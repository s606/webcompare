<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Master</title>
	<link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

	<section class="section">
		<div class="container">
			<nav class="navbar" role="navigation" aria-label="main navigation">
				<div class="navbar-menu">
					<div class="navbar-start">
						<a class="navbar-item" href="/blog">
							Blog (CRUD)
						</a>

						<a class="navbar-item" href="/file">
							File
						</a>

						<a class="navbar-item" href="/image">
							Image
						</a>

					</div>

				</div>
			</nav>
			<hr>





			<a href="/file/create" class="button is-primary is-small">Create/Edit</a>
			<form action="/file/delete" method="POST" style="display: inline;">
				<button type="submit" class="button is-danger is-small">Delete</button>
			</form>

			<hr>
			<div class="field">
				<div class="control">
					<textarea class="textarea" disabled><?= $data ? $data : 'No data found' ?></textarea>
				</div>
			</div>
		</div>
	</section>
</body>

</html>