<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Master</title>
	<link rel="stylesheet" href="/assets/css/bulma.min.css">
</head>

<body>

	<section class="section">
		<div class="container">
			<nav class="navbar" role="navigation" aria-label="main navigation">
				<div class="navbar-menu">
					<div class="navbar-start">
						<a class="navbar-item" href="/blog">
							Blog (CRUD)
						</a>

						<a class="navbar-item" href="/file">
							File
						</a>

						<a class="navbar-item" href="/image">
							Image
						</a>

					</div>

				</div>
			</nav>
			<hr>






			<a href="/blog/create" class="button is-primary is-small">Create</a>
			<hr>
			<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Body</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($blogs as $key => $value) {
						# code...
					?>
						<tr>
							<td><?php echo ($value['id']); ?></td>
							<td><?php echo ($value['title']); ?></td>
							<td><?php echo ($value['body']); ?></td>
							<td width="150px">
								<a href="/blog/edit?id=<?php echo ($value['id']); ?>" class="button is-warning is-small">Edit</a>
								<form action="/blog/delete" method="post" style="display: inline;">
									<input type="hidden" name="id" value="<?= $value['id'] ?>" />
									<button type="submit" class="button is-danger is-small">Delete</button>
								</form>
							</td>
						</tr>

					<?php
					}
					?>

				</tbody>
			</table>















		</div>
	</section>
</body>

</html>