<?php

namespace App\Models;

use CodeIgniter\Model;

class Blog extends Model
{
    protected $table = 'blog';
    protected $allowedFields = ['title', 'body'];
    protected $primaryKey = 'id';

    public function getBlogs()
    {
        return $this->findAll();
    }
}
