<?php
// src/Controller/FileController.php
namespace App\Controller;

use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class FileController extends AbstractController
{
    /**
     * @Route("/main-file", name="file.index")
     */
    public function fileIndex()
    {
        $filesystem = new Filesystem();
        
        
        $filename = 'file.txt';
        $data = false;
        if($filesystem->exists([$filename])){
            $data = file_get_contents("file.txt");
        }

        return $this->render('file/index.html.twig', [
            'data' => $data,
        ]);
    }
    
    /**
     * @Route("/main-file/delete", name="file.delete")
     */
    public function fileDelete()
    {
        $filesystem = new Filesystem();
        
        
        $filename = 'file.txt';
        $data = false;
        if($filesystem->exists([$filename])){
            $filesystem->remove($filename);
        }
        exit();

        // return $this->redirectToRoute('file.index');
    }
    
    /**
     * @Route("/main-file/create-update", name="file.create")
     */
    public function fileCreate()
    {
        $filesystem = new Filesystem();
        
        $filename = 'file.txt';
        $data = false;
        if($filesystem->exists([$filename])){
            $data = file_get_contents("file.txt");
        }

        return $this->render('file/create.html.twig', [
            'data' => $data,
        ]);
    }
    
    /**
     * @Route("/main-file/store", name="file.store")
     */
    public function fileStore()
    {
        $request = Request::createFromGlobals();
        $filesystem = new Filesystem();
        $data = $request->request->get('content');
        
        if($data){
            $filesystem->dumpFile('file.txt', $data);
        }
        exit();

        // return $this->redirectToRoute('file.index');
    }
}
