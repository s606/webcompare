<?php
// src/Controller/BlogController.php
namespace App\Controller;

use App\Entity\Blog;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->redirectToRoute('blog.index');
    }

    /**
     * @Route("/blog", name="blog.index")
     */
    public function blogIndex()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $records = $entityManager->getRepository("App\Entity\Blog")->findAll();

        return $this->render('blog/index.html.twig', [
            'records' => $records,
        ]);
    }

    /**
     * @Route("/blog/json", name="blog.json")
     */
    public function blogJson()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $records = $entityManager->getRepository("App\Entity\Blog")->findAll();

        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new 
        JsonEncoder()));
        $json = $serializer->serialize($records, 'json');
        return new Response($json);
    }

    /**
     * @Route("/blog/create", name="blog.create")
     */
    public function blogCreate()
    {
        $filesystem = new Filesystem();


        return $this->render('blog/create.html.twig');
    }

    /**
     * @Route("/blog/store", name="blog.store")
     */
    public function blogStore()
    {
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();

        $title = $request->request->get('title');
        $body = $request->request->get('body');

        $blog = new Blog();
        $blog->setTitle($title);
        $blog->setBody($body);

        $entityManager->persist($blog);
        $entityManager->flush();
        exit();
        // return $this->redirectToRoute('blog.index');
    }

    /**
     * @Route("/blog/edit/{id}", name="blog.edit")
     */
    public function blogEdit(int $id)
    {
        $blog = $this->getDoctrine()->getRepository(Blog::class)->find($id);
        return $this->render('blog/edit.html.twig', [
            'blog' => $blog,
        ]);
    }

    /**
     * @Route("/blog/update/{id}", name="blog.update")
     */
    public function blogUpdate(int $id)
    {
        $request = Request::createFromGlobals();
        $blog = $this->getDoctrine()->getRepository(Blog::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();

        $title = $request->request->get('title');
        $body = $request->request->get('body');

        $blog->setTitle($title);
        $blog->setBody($body);

        $entityManager->persist($blog);
        $entityManager->flush();
        exit();

        // return $this->redirectToRoute('blog.index');
    }


    /**
     * @Route("/blog/delete/{id}", name="blog.delete")
     */
    public function blogDelete(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $blog = $this->getDoctrine()->getRepository(Blog::class)->find($id);

        $entityManager->remove($blog);
        $entityManager->flush();
        exit();

        // return $this->redirectToRoute('blog.index');
    }
}
