<?php
// src/Controller/ImageController.php
namespace App\Controller;

use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageController extends AbstractController
{
    /**
     * @Route("/main-image", name="image.index")
     */
    public function imageIndex()
    {
        $finder = new Finder();
        $finder->files()->in('images');
        // check if there are any search results
        if ($finder->hasResults()) {
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRelativePathname();
            // ...
        }
        
        return $this->render('image/index.html.twig', [
            'files' => $files,
        ]);
    }
    /**
     * @Route("/main-image/store", name="image.store")
     */
    public function imageStore(Request $request)
    {
        $uploadedFile = $request->files->get('image');
        $destination = $this->getParameter('kernel.project_dir').'/public/images';
        $fileName = $uploadedFile->getClientOriginalName(); 
        $uploadedFile->move($destination, $fileName);
        exit();
        
        // return $this->redirectToRoute('image.index');
    }
    /**
     * @Route("/main-image/delete/{id}", name="image.delete")
     */
    public function imageDelete(String $id)
    {
        $filesystem = new Filesystem();

        $filename = 'images/' . $id;
        if($filesystem->exists([$filename])){
            $filesystem->remove($filename);
        }
        exit();

        // return $this->redirectToRoute('image.index');
    }
}
