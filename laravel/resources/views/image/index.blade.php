
@extends('layout')

@section('body')
<form action="{{route('image.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <h1>Image Upload</h1>
    <div class="field">
        <label class="label">Image</label>
        <div class="control">
            <input class="input" type="file" name="image">
        </div>
      </div>
      <div class="field is-grouped">
        <div class="control">
            <button type="submit" class="button is-success">Save</button>
        </div>
      </div>

</form>

<hr>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
    <thead>
        <tr>
            <th>File Name</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($files as $file)
        <tr>
            <td>{{$file}}</td>
            <td>
                <a href="{{route("image.show")}}?path={{$file}}" target="_blank" class="button is-info is-small">Show</a>
                <form action="{{route('image.destroy')}}?path={{$file}}" method="POST" style="display: inline;">
                    @csrf
                    @method('delete')
                    <button type="submit" class="button is-danger is-small">Delete</button>
                </form>
            </td>
        </tr>

        @endforeach


    </tbody>
</table>
@endsection
