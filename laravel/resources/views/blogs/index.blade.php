@extends('layout')

@section('body')
<a href="{{route('blog.create')}}" class="button is-primary is-small">Create</a>
<hr>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Body</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($Blogs as $Blog)
        <tr>
            <td>{{$Blog->id}}</td>
            <td>{{$Blog->title}}</td>
            <td>{{$Blog->body}}</td>
            <td width="150px">
                <a href="{{route('blog.edit', $Blog->id)}}" class="button is-warning is-small">Edit</a>
                <form action="{{route('blog.destroy', $Blog->id)}}" method="POST" style="display: inline;">
                    @csrf
                    @method('delete')
                    <button type="submit" class="button is-danger is-small">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
@endsection
