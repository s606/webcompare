<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Master</title>
    <link rel="stylesheet" href="{{url('css/bulma.min.css')}}">
</head>

<body>

    <section class="section">
        <div class="container">
            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="{{route('blog.index')}}">
                            Blog (CRUD)
                        </a>

                        <a class="navbar-item" href="{{route('file.index')}}">
                            File
                        </a>

                        <a class="navbar-item" href="{{route('image.index')}}">
                            Image
                        </a>

                    </div>

                </div>
            </nav>
            <hr>




            @yield('body')
        </div>
    </section>
</body>

</html>
