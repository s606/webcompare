@extends('layout')

@section('body')
    <a href="{{route('file.index')}}" class="button is-primary is-small">Back</a>
    <hr>
    <form action="{{route('file.store')}}" method="POST">
        @csrf

        <div class="field">
            <label class="label">Text File</label>
            <div class="control">
                <textarea class="textarea" name="content" rows="15">{{$data ? $data : ''}}</textarea>

            </div>
        </div>
        <button type="submit" class="button is-link">Save</button>
    </form>

@endsection
