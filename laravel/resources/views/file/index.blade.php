@extends('layout')

@section('body')
<a href="{{route('file.create')}}" class="button is-primary is-small">Create/Edit</a>
<form action="{{route('file.destroy')}}" method="POST" style="display: inline;">
    @csrf
    @method('delete')
    <button type="submit" class="button is-danger is-small">Delete</button>
</form>

<hr>
<div class="field">
    <div class="control">
        <textarea class="textarea" disabled>{{$data ? $data : 'No data found'}}</textarea>
    </div>
</div>

@endsection
