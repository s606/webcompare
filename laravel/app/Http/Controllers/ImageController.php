<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = Storage::allFiles('images');

        return view('image.index', compact('files'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => ['required', 'file', 'mimes:jpeg,png,jpg'],
        ]);

        if ($request->has('image') != null) {
            $folderPath = "images";
            $fileName = Str::uuid() . "." . $request->image->getClientOriginalExtension();
            $request->image->storeAs($folderPath, $fileName);
        }

        // return redirect()->route('image.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $path = request()->path;
        $data = null;
        if(Storage::exists($path)){
            $data = Storage::get($path);
        }else{
            abort(404);
        }

        $mimeType = Storage::mimeType($path);

        return response($data, 200)
        ->header('Content-Type', $mimeType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $path = request()->path;
        if(Storage::exists($path)){
            Storage::delete($path);
        }else{
            abort(404);
        }
        // return redirect()->route('image.index');

    }
}
