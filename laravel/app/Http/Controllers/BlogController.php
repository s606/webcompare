<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Blogs = Blog::all();
        return view('blogs.index', compact('Blogs'));
    }


    public function json()
    {
        //
        $Blogs = Blog::all();
        return response()->json($Blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'body' => ['required'],
            'title' => ['required'],
        ]);
        Blog::create($validatedData);
        // return redirect()->route('blog.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Blog = Blog::findOrFail($id);
        return view('blogs.edit', compact('Blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Blog = Blog::findOrFail($id);
        $validatedData = $request->validate([
            'body' => ['required'],
            'title' => ['required'],
        ]);
        $Blog->update($validatedData);
        // return redirect()->route('blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Blog = Blog::findOrFail($id);
        $Blog->delete();
        // return redirect()->route('blog.index');
    }
}
