<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('blog.index');
});

Route::get('/file', 'FileController@index')->name('file.index');
Route::delete('/file', 'FileController@destroy')->name('file.destroy');
Route::get('/file/create-update', 'FileController@create')->name('file.create');
Route::post('/file', 'FileController@store')->name('file.store');

Route::get('/image','ImageController@index')->name('image.index');
Route::get('/image/show','ImageController@show')->name('image.show');
Route::delete('/image','ImageController@destroy')->name('image.destroy');
Route::post('/image','ImageController@store')->name('image.store');

Route::resource('blog', 'BlogController')->except(['show']);
Route::get('/blog/json','BlogController@json')->name('blog.json');
